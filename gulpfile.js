const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const minifyImg = require('gulp-imagemin');
const minifyJS = require('gulp-uglify');
const minifyHTML = require('gulp-htmlmin');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const runSequence = require('run-sequence');

gulp.task('css', ()=>{
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'nested',
            precision: 10,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(autoprefixer())
        .pipe(concat('mtmslider_all.css'))
        .pipe(gulp.dest('src/css'));
});

gulp.task('js', () => {
    return gulp.src('src/sourcejs/**/*.js')
        .pipe(concat('mtmslider_all.js'))
        .pipe(minifyJS())
        .pipe(gulp.dest('src/js'));
});

gulp.task('watch', () => {
    gulp.watch("src/scss/**/*.scss", gulp.series('css'));
    gulp.watch("src/sourcejs/**/*.js", gulp.series('js'));
});

gulp.task('default', () => {
    runSequence(
        'css',
        'js',
        'watch'
    );
});