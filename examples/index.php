<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Mavitm Layer Slider</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../src/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="../src/css/mtmSliderAll.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php /**/?>
<div id="boxSlide">
    <ul id="mavitmSlider">
        <?php /**/?>
        <li><a href="#"><img src="slide/1/redBack.jpg" alt="" /></a></li>

        <li><a href="#"><img src="slide/2/blueBack.jpg" alt="" /></a>
            <div class="captionArea">

                <div class="caption mtmCol100 fullImg"
                     data-in-speed="8000ms"
                     data-in-effect="fadeInLeft"
                     data-out-speed="5000ms"
                     data-out-effect="fadeOutRight"
                     data-in-delay="0ms"
                     data-out-delay="15000ms"
                     data-position-x="0"
                     data-position-y="0"
                     data-position-z="1"><img src="slide/2/cloud_one.png" alt="cloud" /></div>

                <div class="caption mtmCol100 fullImg"
                     data-in-speed="8000ms"
                     data-in-effect="fadeInRight"
                     data-out-speed="5000ms"
                     data-out-effect="fadeOutLeft"
                     data-in-delay="300ms"
                     data-out-delay="15000ms"
                     data-position-x="0"
                     data-position-y="0"
                     data-position-z="1"><img src="slide/2/cloud_two.png" alt="cloud" /></div>

                <div class="caption mtmCol100 fullImg"
                     data-in-speed="7500ms"
                     data-in-effect="fadeIn"
                     data-out-speed="5000ms"
                     data-out-effect="fadeOutUp"
                     data-in-delay="500ms"
                     data-out-delay="15000ms"
                     data-position-x="0"
                     data-position-y="0"
                     data-position-z="1"><img src="slide/2/cloud_three.png" alt="cloud" /></div>



                <div class="caption mtmCol25 fullImg"
                     data-in-speed="750ms"
                     data-in-effect="bounceInUp"
                     data-out-speed="750ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="8000ms"
                     data-out-delay="13500ms"
                     data-position-x="75"
                     data-position-y="80"
                     data-position-z="1"><img src="slide/2/green.png" alt="green" /></div>

                <div class="caption mtmCol15 fullImg"
                     data-in-speed="750ms"
                     data-in-effect="bounceInUp"
                     data-out-speed="750ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="8300ms"
                     data-out-delay="13200ms"
                     data-position-x="65"
                     data-position-y="90"
                     data-position-z="1"><img src="slide/2/green.png" alt="green" /></div>

                <div class="caption mtmCol35 fullImg"
                     data-in-speed="750ms"
                     data-in-effect="bounceInUp"
                     data-out-speed="750ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="8600ms"
                     data-out-delay="12900ms"
                     data-position-x="0"
                     data-position-y="75"
                     data-position-z="1"><img src="slide/2/green.png" alt="green" /></div>

                <div class="caption mtmCol20 fullImg"
                     data-in-speed="750ms"
                     data-in-effect="bounceInUp"
                     data-out-speed="750ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="8900ms"
                     data-out-delay="12600ms"
                     data-position-x="30"
                     data-position-y="85"
                     data-position-z="1"><img src="slide/2/green.png" alt="green" /></div>

                <div class="caption mtmCol15 fullImg"
                     data-in-speed="750ms"
                     data-in-effect="rotateInUpRight"
                     data-out-speed="750ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="9100ms"
                     data-out-delay="12400ms"
                     data-position-x="50"
                     data-position-y="50"
                     data-position-z="1"><img src="slide/2/tree.png" alt="green" /></div>

                <div class="caption mtmCol50 fullImg"
                     data-in-speed="1000ms"
                     data-in-effect="bounceInRight"
                     data-out-speed="1000ms"
                     data-out-effect="bounceOutLeft"
                     data-in-delay="9300ms"
                     data-out-delay="12100ms"
                     data-position-x="25"
                     data-position-y="15"
                     data-position-z="1"><img src="slide/2/ucak.png" alt="green" /></div>

                <div class="caption mtmCol33 text-center mtmFontSize3 mtmColor2"
                     data-in-speed="1000ms"
                     data-in-effect="flipInX"
                     data-out-speed="1000ms"
                     data-out-effect="flipOutX"
                     data-in-delay="25000ms"
                     data-out-delay="8000ms"
                     data-position-x="33"
                     data-position-y="25"
                     data-position-z="5">A more recent post</div>

                <div class="caption mtmCol20 text-center mtmFontSize2 mtmCbox3 mtmPadding15"
                     data-in-speed="1000ms"
                     data-in-effect="bounceIn"
                     data-out-speed="1000ms"
                     data-out-effect="bounceOut"
                     data-in-delay="25500ms"
                     data-out-delay="8000ms"
                     data-position-x="40"
                     data-position-y="40"
                     data-position-z="5">More information</div>

            </div>
        </li>

        <?php /**/?>
    </ul>
</div>

<div id="proces" style="
position: fixed;
left: 0;
bottom: 15px;
width: 100%;
max-height: 480px;
overflow: auto;
color: #ffffff;
padding: 15px;
background-color: #0f0f0f;
"></div>

<?php /**/?>

<script type="text/javascript" src="../src/js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript" src="../src/js/jquery.mtmAttributes.js"></script>
<script type="text/javascript" src="../src/js/jquery.mtmLayer.js"></script>
<script type="text/javascript" src="../src/js/jquery.mtmMouseEvents.js"></script>

<script type="text/javascript" src="../src/js/jquery.mtmSlider.js"></script>
    <script type="text/javascript">
        $(window).ready(function(){
            var mtmSLide = $("#mavitmSlider").mavitmSlider({
                pauseTime:5000,
                touchControl:false
            });
        });
    </script>
</body>
</html>