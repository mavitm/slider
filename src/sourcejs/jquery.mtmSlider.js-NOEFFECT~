/**
 * Site :mavitm.com
 * Yazar : Ayhan ERASLAN
 * Email : ayer50@gmail.com
 * @param {Object} $
 */
(function($,undefined){
    "use strict";
    $.fn.mavitmSlider = function(tConf) {
        var element = this;

        element.defaults = {
            defaultEffect: 'random',
            cssAnimate: 'random',
            animSpeed: 1000,
            standbyTime: 3000,
            autoSlide: true,
            pauseOnHover: true,
            touchControl: true,
            controlNav: true,
            previousLink: '.slidePrevious',
            nextLink: '.slideNextLink',
            /*ACTIVE*/
            currentindex: 0,

            vSlices: 4,
            hSlices: 15,

            setupCall: function () {},
            resizeCall: function () {},
            changeBeforeCall: function () {},
            changeAfterCall: function () {},
        };

        element.tConf = $.extend(element.defaults, tConf);

        element.timer = null;

        element.currentindex = 0;

        element.currentHeight = 0;

        element.pause = 0;

        element.childs = new Array;

        element.totalChilds = 0;

        element.slideEffekt = ['slideFade', 'slideZoomOut', 'slideMosaicLeft', 'slideMosaicRight', 'slideMosaicCenter', 'slideMosaicShuffle', 'slideVertical', 'slideHorziontal'];

        element.animateInClasses = ['flipInX', 'flipInY', 'fadeIn', 'fadeInUp', 'fadeInDown', 'fadeInLeft', 'fadeInRight', 'fadeInUpBig', 'fadeInDownBig', 'fadeInLeftBig', 'fadeInRightBig', 'bounceIn', 'bounceInDown', 'bounceInUp', 'bounceInLeft', 'bounceInRight', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'lightSpeedIn', 'slideInUp', 'slideInDown', 'slideInLeft', 'slideInRight', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp'];

        element.animateOutClasses = ['flipOutX', 'flipOutY', 'fadeOut', 'fadeOutUp', 'fadeOutDown', 'fadeOutLeft', 'fadeOutRight', 'fadeOutUpBig', 'fadeOutDownBig', 'fadeOutLeftBig', 'fadeOutRightBig', 'bounceOut', 'bounceOutDown', 'bounceOutUp', 'bounceOutLeft', 'bounceOutRight', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'hinge', 'rollOut', 'lightSpeedOut', 'slideOutUp', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp'];

        element.slideStage = false;

        element.startCaption = false;

        /**
         * create html and refactor process
         * @returns {boolean}
         */
        element.build = function () {
            if (typeof element == "undefined") {
                selfLog("MaviTm slider failed init");
                return false;
            }

            element.pages = $('<ul class="mtmPager mtmSlideHover"></ul>');
            element.arrows = $('<div class="mtmControls mtmSlideHover"></div>');

            if (!element.parent().hasClass("mavitmSlider")) {
                element.wrapper = 'mavitm' + Math.floor((Math.random() * 999) + 1);
                element.wrap('<div class="mavitmSlider" id="' + element.wrapper + '"></div>');
                $("#" + element.wrapper).css({position: "relative"});

                element.arrows.append('<a class="mtmControl mtmPrevious"></a>');
                element.arrows.append('<a class="mtmControl mtmNext"></a>');
                $("#" + element.wrapper).append(element.arrows);
                $("#" + element.wrapper).append('<div class="mtmLoader"></div>');
            }

            var childs = element.children();
            element.totalChilds = (childs.size() - 1);

            if (element.tConf.currentindex > element.totalChilds || element.tConf.currentindex < 1) {
                element.tConf.currentindex = 0;
            }
            element.currentindex = element.tConf.currentindex;

            var cx = 0;
            childs.each(function (i) {
                var child = $(this), img, url, captions;

                if (child.is('img')) {
                    img = child;
                    url = '#';
                } else if (child.is('a')) {
                    img = child.find("img:first");
                    url = child.attr("href");
                } else {
                    img = child.find("img:first");
                    url = (child.find("a:first").length != 0 ? child.find("a:first").attr("href") : '#');
                }

                captions = (child.find(".captionArea:first").length != 0 ? child.find(".captionArea:first") : null);

                if (img.prop("tagName").toLowerCase() !== "img") {
                    child.remove();
                } else {
                    element.childs[cx] = {
                        "img": img,
                        "url": url,
                        "src": img.attr("src"),
                        'iwidth': img.width(),
                        'iheight': img.height(),
                        'captions': captions,
                        'captioninit': null
                    };

                    img.css({"width": "100%"});

                    child.attr("data-width", element.childs[cx].iwidth)
                        .attr("data-height", element.childs[cx].iheight)
                        .attr("data-src", element.childs[cx].src)
                        .attr("data-href", element.childs[cx].url)
                        .css({width: '100%'});

                    if (element.currentindex === cx) {
                        child.css({'display': 'block'}).addClass('active');
                    } else {
                        child.css({'display': 'none'}).removeClass('active');
                    }
                    cx++;
                    element.pages.append('<li><a>' + cx + '</a></li>');
                }
            });

            if (element.tConf.controlNav !== true) {
                element.pages.css({'display': 'none'});
            }
            if ($("#" + element.wrapper).find(".mtmPager").length < 1) {
                $("#" + element.wrapper).append(element.pages);
            }

            if (typeof element.childs[element.currentindex].width != undefined) {
                element.tConf.setupCall(element);
                element.resizing();
            }
        };

        element.init = function()
        {
            element.build();

            try {
                var loadImg = new Image();
                loadImg.src = element.childs[element.currentindex].src;

                $(loadImg).one("load", function () {
                    element.hiddenLoader();
                });
            }
            catch (e) {
                element.hiddenLoader();
            }

            $(window).resize(function(){
                element.resizing();
                element.tConf.resizeCall(element);
            });

            if(element.tConf.controlNav == true){
                $("#"+element.wrapper).hover(
                    function(){ $("#"+element.wrapper).find(".mtmSlideHover").stop(true,true).fadeIn("normal"); },
                    function(){ $("#"+element.wrapper).find(".mtmSlideHover").stop(true,true).fadeOut("normal"); }
                );
            }

            return element;
        };

        element.resizing = function()
        {
            var currenctIndex = element.currentindex;
            if($("li", element).eq(currenctIndex).is(":hidden") == true)
            {
                currenctIndex = $("li.active:first",element).index();
            }
            var iWidth      = element.childs[currenctIndex].iwidth;
            var iHeight     = element.childs[currenctIndex].iheight;
            var imgWidth    = element.childs[currenctIndex].img.width();

            element.currentHeight = getHeight(iWidth, iHeight, imgWidth);

            element.css({overflow:"hidden",listStyle:"none",width: "100%",height:element.currentHeight});
            element.parent(".mavitmSlider").css({overflow: "hidden",width: "100%",height:element.currentHeight});
            element.attr("data-width",element.width()).attr("data-height",element.height());

            selfLog("resizing");
        };

        element.hiddenLoader = function () {
            $("#"+element.wrapper).find('.mtmLoader').css({"display":"none"});
        };

        element.showLoader = function () {
            $("#"+element.wrapper).find('.mtmLoader').css({"display":"block"});
        };

        element.slideStart = function()
        {

        };

        element.slideEnd = function()
        {

        };

        element.autoSlide = function()
        {
            clearTimeout(element.timer);

            if(element.tConf.autoSlide !== true){
                return false;
            }

            if(element.pause > 0 || element.startCaption === true)
            {
                element.timer = setTimeout(function () {
                    element.autoSlide();
                }, element.standbyTime);
            }
            else
            {
                element.timer = setTimeout(function () {
                    element.slideStart();
                }, element.tConf.standbyTime);
            }
        };

        element.setSlideStage = function(boll)
        {
            element.slideStage = boll;

            if(boll === true)
            {
                clearTimeout(element.timer);
                element.tConf.changeBeforeCall(element);
            }
            else
            {
                element.resizing();
                element.tConf.changeAfterCall(element);
                element.autoSlide();
            }

        };

        element.playProcess = function(){
            if(element.pause > 0){
                element.pause = 0;
                element.autoSlide();
            }
        };

        element.pauseProcess = function(){
            if(element.pause < 1) {
                element.pause = 1;
                clearTimeout(element.timer);
            }
        };

        return element.init();

        function selfLog(log)
        {
            console.log(log);
        }

        function getHeight(x,y,z)
        {
            /* (x(width) - y(height)) | (z(width) - n(? height)) */
            return ((y * z) / x);
        }

        function shuffle (o)
        {
            for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        }
        function array_keys(o){
            o = shuffle(o);
            var r = new Array(0);
            for (var i in o){
                r.push(i);
            }
            return r;
        }
    }
})(jQuery);