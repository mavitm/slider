/**
 * Site :mavitm.com
 * Yazar : Ayhan ERASLAN
 * Email : ayer50@gmail.com
 * @param {Object} $
 */
(function($,undefined){
    "use strict";

    $.fn.mtmMouseMoveEvent = function(tConf){
        var that = this;
        var defaults = {
            "upCall":function(){}, 		/*mouseUp*/
            "downCall":function(){}, 	/*mouseDown*/
            "moveCall":function(){}, 	/*mouseMove*/
            "leftCall":function(){},
            "rightCall":function(){},
            "topCall":function(){},
            "bottomCall":function(){},
        };

        that.tConf = $.extend(defaults, tConf);

        that.moveStart = 0;
        that.startX = 0;
        that.startY = 0;
        that.endX = 0;
        that.endY = 0;
        that.sizeX = 0;
        that.sizeY = 0;
        /**
         * mesafe belirtilen degerden küçük ise islem yapma | Events are not affected if the distance is less than the specified value
         * @type {number}
         */
        that.xIgnorePixelGap = 280;
        that.leftRight = undefined;
        that.upDown = undefined;
        that.up = {
            "element" : undefined
        };
        that.down = {
            "element" : undefined
        };

        that.initialize = function(){

            that.mousedown(function(e){
                that.down.element = $(this);
                that.down.x = that.down.element.offset().top;
                that.down.y = that.down.element.offset().left;
                that.moveStart = 1;
                that.startX = e.pageX;
                that.startY = e.pageY;
                that.leftRight = undefined;
                that.upDown = undefined;
                that.sizeX = 0;
                that.sizeY = 0;
                that.tConf.downCall(that);
            });

            that.mouseup(function(e){
                that.up.element = $(this);
                that.up.x = that.up.element.offset().top;
                that.up.y = that.up.element.offset().left;
                that.moveStart = 0;
                that.endX = e.pageX;
                that.endY = e.pageY;
                that.leftRight = that.endX > that.startX ? 'right' : 'left';
                that.upDown = that.endY > that.startY ? 'down' : 'up';
                that.sizeX = (that.startX - that.endX);
                that.sizeY = (that.startY - that.endY);

                that.tConf.upCall(that);

                (that.upDown == 'down' ? that.tConf.bottomCall(that) : that.tConf.topCall(that));

                if((Math.max(that.startX, that.endX) - Math.min(that.startX, that.endX)) >= that.xIgnorePixelGap){
                    (that.leftRight == 'left' ? that.tConf.leftCall(that) : that.tConf.rightCall(that));
                }

            });

            that.mousemove(function(e){

                if(that.moveStart < 1){
                    return false;
                }

                /*
                 that.endX = e.pageX;
                 that.endY = e.pageY;
                 */
                that.leftRight = e.pageX > that.startX ? 'right' : 'left';
                that.upDown = e.pageY > that.startY ? 'down' : 'up';
                that.sizeX = (that.startX - e.pageX);
                that.sizeY = (that.startY - e.pageY);


                that.tConf.moveCall(that);
            });

            return that;
        };

        return that.initialize();
    };

})(jQuery);