/**
 * Site :mavitm.com
 * Yazar : Ayhan ERASLAN
 * Email : ayer50@gmail.com
 * @param {Object} $
 */
(function($,undefined){
    "use strict";
    $.fn.getAttributes = function () {
        var elem = this,
            attr = {};
        if(elem && elem.length) {
            $.each(elem.get(0).attributes, function( v, n ) {
                n = n.nodeName || n.name;
                v = elem.attr(n);
                if( v !== undefined && v !== false ) attr[n] = v;
            });
        }
        return attr
    };
})(jQuery);