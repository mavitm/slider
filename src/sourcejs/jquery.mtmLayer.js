/**
 * Site :mavitm.com
 * Yazar : Ayhan ERASLAN
 * Email : ayer50@gmail.com
 * @param {Object} $
 */
(function($,undefined){
    "use strict";

    $.fn.mavitmLayers = function(eConf){
        var defaults = {
            "selector" : ".caption",
            "layerPosition":"absolute",
            "endCall" : function(){},
            "stopCall": function () {}
        };
        var that = this;
        that.tConf = $.extend(defaults, eConf);
        that.totalTime = 0;
        that.layerCount = 0;
        that.procesCount = 0;
        that.allLayers = {};
        /**
         * butun islemleri ve akısi durdur ve endCall islemini tetikleme
         * @type {boolean}
         */
        that.absolutelyStop = false;

        that.countUpdate = function(){
            that.procesCount += 1;
            if(that.procesCount >= that.layerCount){
                if(that.absolutelyStop !== true){
                    that.tConf.endCall(that);
                }
            }
        };

        that.initialize = function(){

            that.layerCount = 0;
            that.procesCount = 0;
            that.totalTime = 0;
            that.allLayers = {};

            $(that.tConf.selector,that).each(function(i){
                var captionNode = $(this).mavitmLayerAnimate({
                    autoStart:true,
                    layerPosition: that.tConf.layerPosition,
                    afterCall: function(){
                        that.countUpdate();
                    }
                });
                that.totalTime += captionNode.allTimer;
                that.layerCount += 1;
                captionNode.attr("data-duration", captionNode.allTimer);
                that.allLayers[i] = captionNode;
            });

            return that;
        };

        that.stopAt = function () {
            that.absolutelyStop = true;
            if(Object.keys(that.allLayers).length > 0){
                /*for (let [key, value] of Object.entries(that.allLayers))*/
                for(var i in that.allLayers){
                    try{
                        that.allLayers[i].destroy();
                    }catch (e) {}
                }
            }
            that.tConf.stopCall(that);
        };

        return that.initialize();
    };

    $.fn.mavitmLayerAnimate = function(eConf){
        var defaults = {
            "in":{
                "speed":"1000ms",
                "effect":"bounce",
                "delay":0,
                "repeat":false
            },
            "out":{
                "speed":"1000ms",
                "effect":"wobble",
                "delay":"2000ms",
                "repeat":false
            },
            "position":{
                "x":0,
                "y":0,
                "z":1
            },
            "textsplit":false,
            "textSplitDuration" : 75,
            "layerPosition":"absolute",
            "autoStart":false,
            "textAnimate":false,
            "beforeCall" : function(){},
            "afterCall" : function(){}
        };

        var cLayer = this;
        this.tConf = $.extend(defaults, eConf);
        this.allTimer = 0;

        this.timeOuts = {
            outDelay : null,
            out: null,
            textSplit: null,
            inDelay: null,
            in: null
        };

        this.initialize = function() {
            this.tConf = $.extend(defaults, getData());
            return this.totalTimer().setCss().autoAnimate();
        };

        this.totalTimer = function(){
            var allTime = [
                cLayer.tConf.in.speed,
                cLayer.tConf.in.delay,
                cLayer.tConf.out.speed,
                cLayer.tConf.out.delay
            ];
            this.allTimer = 0;
            for(var i in allTime){
                this.allTimer += secondToMiliSecond(allTime[i]);
            }
            return cLayer;
        };

        this.setCss = function(){
            this.css({
                position: this.tConf.layerPosition,
                zIndex : this.tConf.position.z,
                top : this.tConf.position.y+"%",
                left : this.tConf.position.x+"%"
            });
            return cLayer;
        };

        this.autoAnimate = function(){
            if(this.tConf.autoStart == true){
                this.runAnimate();
            }
            return cLayer;
        };

        this.runAnimate = function(){
            setAnimate(this,"in");
            /** setAnimate(this,"out");/**/
            return this;
        };

        this.destroy = function () {
            for(var i in this.timeOuts){
                if(this.timeOuts[i] !== null){
                    clearTimeout(this.timeOuts[i]);
                    this.timeOuts[i] = null;
                }
            }
            cLayer.css({display: "none"});
        };

        return this.initialize();

        function stringToBoolean(str) {
            if (str !== "true" && str !== "false") return str;
            return (str === "true");
        }

        function getData(){
            var data = cLayer.getAttributes();
            var availableData = {};

            if( Object.keys(data).length < 1 ) return data;

            for (var i in data){
                var nodeName = i;
                if (/^data-in-*/.test(nodeName)) {
                    availableData.in = availableData.in || {};
                    availableData.in[nodeName.replace(/data-in-/, '')] = stringToBoolean(data[i]);
                } else if (/^data-out-*/.test(nodeName)) {
                    availableData.out = availableData.out || {};
                    availableData.out[nodeName.replace(/data-out-/, '')] =stringToBoolean(data[i]);
                }else if (/^data-position-*/.test(nodeName)) {
                    availableData.position = availableData.position || {};
                    availableData.position[nodeName.replace(/data-position-/, '')] =stringToBoolean(data[i]);
                } else if (/^data-*/.test(nodeName)) {
                    availableData[nodeName.replace(/data-/, '')] = stringToBoolean(data[i]);
                }
            }
            return availableData;
        }

        function setAnimate(l, inOut){

            l.removeClass('animated');
            l.removeClass(l.tConf.out.effect);
            l.removeClass(l.tConf.in.effect);

            if(inOut != "in"){

                l.timeOuts.outDelay = setTimeout(function(){
                    animate(l, l.tConf.out.effect, l.tConf.out.speed, l.tConf.out.repeat);
                },secondToMiliSecond(l.tConf.out.delay));

                l.timeOuts.out = setTimeout(function(){
                    l.tConf.afterCall();
                    l.hide();
                },(secondToMiliSecond(l.tConf.out.delay) + secondToMiliSecond(l.tConf.out.speed)));

            }else{

                if(l.tConf.textsplit === true){

                    l.timeOuts.textSplit = setTimeout(function(){
                        textSplit(l,'','word','');
                    },secondToMiliSecond(l.tConf.in.delay));

                }

                l.timeOuts.inDelay = setTimeout(function(){
                    animate(l, l.tConf.in.effect, l.tConf.in.speed, l.tConf.in.repeat);
                },secondToMiliSecond(l.tConf.in.delay));

                l.timeOuts.in = setTimeout(function(){
                    l.tConf.beforeCall();
                    setAnimate(l,"out");
                },(secondToMiliSecond(l.tConf.in.delay) + secondToMiliSecond(l.tConf.in.speed)));
            }
        }

        function animate(l, effect, speed, repeat){
            if(repeat){
                l.addClass('infinite');
            }
            l.css({
                "webkitAnimationDuration":speed,
                "mozAnimationDuration":speed,
                "MSAnimationDuration":speed,
                "oanimationDuration":speed,
                "animationDuration":speed
            });
            l.addClass('animated')
                .addClass(effect)
                .css('visibility', 'visible')
                .show();

            /**
            l.on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                l.removeClass('animated').removeClass(effect);
                l.off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
            });
            /**/
        }

        function textSplit(t, separator, cssClass, after) {
            var a = t.text().split(separator);
            var rSpHtml = $('<div class="splitTextWrap"></div>');
            rSpHtml.css({minWidth:t.width()+"px", width:"100%"});
            if (a.length) {

                var dis = (a.length / parseFloat(t.tConf.in.speed));
                var dos = (a.length / parseFloat(t.tConf.out.speed));
                var did = (a.length / parseFloat(t.tConf.in.delay));
                var dod = (a.length / parseFloat(t.tConf.out.delay));
                var dodAll = parseFloat(t.tConf.out.delay);
                /*
                 var dodAll = (parseFloat(t.tConf.out.delay) - (a.length * cLayer.tConf.textSplitDuration));
                 if(dodAll < 0){
                 dodAll = (dodAll * -1);
                 }*/

                var x = 0;
                $(a).each(function(i, item) {
                    var chtml = $('<span></span>');
                    chtml.addClass(cssClass)
                        .attr("data-in-speed", cLayer.tConf.textSplitDuration)
                        .attr("data-in-effect", t.attr("data-in-effect"))
                        .attr("data-out-speed", cLayer.tConf.textSplitDuration)
                        .attr("data-out-effect", t.attr("data-out-effect"))
                        .attr("data-in-delay", x+"ms")
                        .attr("data-out-delay", (dodAll - x)+"ms")
                        .attr("data-position-x", 0)
                        .attr("data-position-y", 0)
                        .attr("data-position-z", (i+1))
                        .text(item)
                        .css({display:"none"});
                    rSpHtml.append(chtml);
                    x += cLayer.tConf.textSplitDuration;
                });
                t.empty().html(rSpHtml);
                /**/
                rSpHtml.mavitmLayers({
                    selector:"."+cssClass,
                    layerPosition:"relative"
                });
                /**/
            }
        }

        function secondToMiliSecond(time){
            var nt = time.toLowerCase();
            if(nt[nt.length - 1] == "s" && nt[nt.length - 2] != "m"){
                return (parseFloat(nt) * 1000);
            }
            return parseFloat(nt);
        }
    };

})(jQuery);