/**
 * Site :mavitm.com
 * Yazar : Ayhan ERASLAN
 * Email : ayer50@gmail.com
 * @param {Object} $
 */
(function($,undefined){
    "use strict";

    $.fn.mavitmSlider = function(tConf) {

        var element = this;

        element.defaults = {
            'defaultEffect': 'random',
            'cssAnimate': 'random',
            'animSpeed': '1000',
            'autoSlide': true,
            'pauseOnHover': true,
            'touchControl': true,
            'controlNav': true,
            'previousLink': '.slidePrevious',
            'nextLink': '.slideNextLink',
            /*ACTIVE*/
            'currentindex': 0,

            //animayon y dikey parca sayısı
            'vSlices': 4,
            //animasyon x yatay parca sayısı
            'hSlices': 15,

            'setupCall': function () {},
            'resizeCall': function () {},
            'changeBeforeCall': function () {},
            'changeAfterCall': function () {},
        };
        element.vars = $.extend(element.defaults, tConf);

        element.wrapper = null;

        /**
         * zamanlamayı clear veya setTime için
         * @type {null}
         */
        element.timer = null;

        element.pause = 0;

        element.pauseTime = 3000;

        element.totalChilds = 0;

        element.childsData = [];

        /**
         * slide iceri giriyor
         * @type {boolean}
         */
        element.startAnimasyon = false;

        element.startCaption = false;

        element.oldIndex = 0;

        element.dVars = {
            set runAniamtion(value){
                element.startAnimasyon = value;
            },
            set runCaption(value){
                element.startCaption = value;
            },
            set activeIndex(index){
                element.oldIndex = $("li.active:first", element).index();
                element.setCurrentIndex(index);
            },
        };

        element.slideEffekt = ['slideFade', 'slideZoomOut', 'slideMosaicLeft', 'slideMosaicRight', 'slideMosaicCenter', 'slideMosaicShuffle', 'slideVertical', 'slideHorziontal'];

        element.animateInClasses = ['flipInX', 'flipInY', 'fadeIn', 'fadeInUp', 'fadeInDown', 'fadeInLeft', 'fadeInRight', 'fadeInUpBig', 'fadeInDownBig', 'fadeInLeftBig', 'fadeInRightBig', 'bounceIn', 'bounceInDown', 'bounceInUp', 'bounceInLeft', 'bounceInRight', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'lightSpeedIn', 'slideInUp', 'slideInDown', 'slideInLeft', 'slideInRight', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp'];

        element.animateOutClasses = ['flipOutX', 'flipOutY', 'fadeOut', 'fadeOutUp', 'fadeOutDown', 'fadeOutLeft', 'fadeOutRight', 'fadeOutUpBig', 'fadeOutDownBig', 'fadeOutLeftBig', 'fadeOutRightBig', 'bounceOut', 'bounceOutDown', 'bounceOutUp', 'bounceOutLeft', 'bounceOutRight', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'hinge', 'rollOut', 'lightSpeedOut', 'slideOutUp', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp'];

        element.initialize = function(){
          element.build();
          element.slideInterval();
          return element;
        };

        element.slideInterval = function () {
            clearTimeout(element.timer);
            if(element.vars.autoSlide === true)
            {
                if(element.pause > 0){
                    element.timer = setTimeout(function () {
                        element.slideInterval();
                    }, element.pauseTime);
                }
                else{
                    element.timer = setTimeout(function () {
                        element.nextSlide();
                    }, element.pauseTime);
                }
            }
        };

        element.nextSlide = function(){
            element.dVars.activeIndex = (element.vars.currentindex + 1);
            element.slideStart();
        };

        element.previousSlide = function(){
            element.dVars.activeIndex = (element.vars.currentindex - 1);
            element.slideStart();
        };

        element.gotoSlide = function(index){
            element.dVars.activeIndex = index;
            element.slideStart();
        };

        element.slideStart = function () {
            clearTimeout(element.timer);
            if(element.startAnimasyon === true){
                return false;
            }

            element.fixResize();

            element.dVars.runAniamtion = true;
            var viewItem = $("li", element).eq(element.vars.currentindex);
            var maxW = $(viewItem).data("width"),
                maxH = $(viewItem).data("height"),
                currentW = $(viewItem).width(),
                effect = element.vars.defaultEffect,
                cssAnimate = element.vars.cssAnimate;

            if(typeof $(viewItem).data("animation") != "undefined"){
                effect = $(viewItem).data("animation");
                if(/^css-/.test(effect)){
                    cssAnimate = effect.split('css-')[1];
                    effect = "cssAnimate";
                }else if( $.inArray(effect,element.slideEffekt) < 0 ){
                    effect = element.vars.defaultEffect;
                }
            }

            element.childsData[element.vars.currentindex] = $.extend(element.childsData[element.vars.currentindex], {
                "liindex" : element.vars.currentindex,
                'maxW' : maxW,
                'maxH' : maxH,
                'currentW' : currentW,
                'replaceH' :findHeightProportions(maxW, maxH, currentW),
                'effect' : (
                    effect === "random" ?
                        element.slideEffekt[Math.floor(Math.random()*(element.slideEffekt.length))] : effect
                ),
                'cssAnimate' : (
                    cssAnimate === "random" ?
                        element.animateInClasses[Math.floor(Math.random()*(element.animateInClasses.length))] : cssAnimate
                )
            });

            $("#"+element.wrapper).find(".mtmLoader").css({display:'block'});
            slidelog("start");
            element.chilImgLoader(function () {
                $("#"+element.wrapper).find(".mtmLoader").css({display:'none'});
                effectRun();
            });
        };

        element.slideEnd = function () {
            element.dVars.runAniamtion = false;
            slidelog("slide animation end");

            /*
            * burda slide geçiş tamamlandı caption islemleri veya auto aktif ise diger slide
            */

            if($("li",element).eq(element.vars.currentindex).find(".captionArea").length > 0){
                element.dVars.runCaption = true;
                var child = element.childsData[element.vars.currentindex];
                if(child.captioninit === null){

                    child.captioninit = child.captions.mavitmLayers({
                        selector : '.caption',
                        endCall : function(e){
                            if(element.pause > 0 || element.vars.autoSlide === false){
                                /* slide pause halinde veya autoSlide false ise sonrakine geçme caption replay yap */
                                e.initialize();
                            }else{
                                element.captionEnd();
                            }
                        },
                        stopCall: function (e) {
                            child.captioninit = null;
                            element.captionEnd();
                        }
                    });
                }else{
                    child.captioninit.initialize();
                }
            }
            else{
                element.slideInterval();
            }
        };

        element.captionEnd = function(){
            element.dVars.runCaption = false;
            element.slideInterval();
        };

        element.fixResize = function(){
            var sx = element.vars.currentindex;
            if(typeof element.childsData[sx].width !== undefined){
                element.css({
                    overflow:"hidden",
                    listStyle:"none",
                    width: "100%",
                    height:findHeightProportions(element.childsData[sx].iwidth, element.childsData[sx].iheight, element.width())
                });
                element.parent(".mavitmSlider").css({
                    overflow: "hidden",
                    width: "100%",
                    height:findHeightProportions(element.childsData[sx].iwidth, element.childsData[sx].iheight, element.width())
                });
                element.attr("data-width",element.width()).attr("data-height",element.height());
            }else{
                slidelog("MaviTm slider resize failed ");
            }
        };

        element.playProcess = function(){
            element.pause = 0;
            if(element.vars.autoSlide === true){
                if(element.startAnimasyon !== true && element.startCaption !== true){
                    element.slideInterval();
                }
            }
        };

        element.pauseProcess = function(){
            element.pause = 1;
            clearTimeout(element.timer);
        };

        element.setCurrentIndex = function(index){
          element.vars.currentindex = (index % ( element.totalChilds + 1 ));
          if(element.vars.currentindex < 0){
              element.vars.currentindex = 0;
          }
        };

        element.chilImgLoader = function(callback){
            /**$("#"+element.wrapper).find(".mtmLoader").css({display:'block'});**/
            var child = element.childsData[element.vars.currentindex];
            var lImg = new Image();
            if(typeof child.src != "undefined"){
                lImg.src = child.src;
                $(lImg).off("load").on("load", function () {
                    callback();
                });
            }
            else{
                callback();
            }
        };

        element.build = function() {
            if(typeof element == "undefined"){ return; }

            if (!element.parent().hasClass("mavitmSlider")) {

                element.wrapper = 'mavitm' + Math.floor((Math.random() * 999) + 1);
                element.wrap('<div class="mavitmSlider" id="' + element.wrapper + '"></div>');
                $("#" + element.wrapper).css({position: "relative"});

                element.pages = $('<ul class="mtmPager mtmSlideHover" id="' + element.wrapper + '-pages"></ul>');
                element.arrows = $('<div class="mtmControls mtmSlideHover" id="' + element.wrapper + '-arrows"></div>');

                element.arrows.append('<a class="mtmControl mtmPrevious"></a>');
                element.arrows.append('<a class="mtmControl mtmNext"></a>');
                $("#" + element.wrapper).append(element.arrows)
                    .append('<div class="mtmLoader"></div>');
            }

            element._prepareChilds(function (childs) {
                if(childs.size() > 0) {
                    if ($("#" + element.wrapper).find(".mtmPager").length < 1 && element.vars.controlNav === true) {
                        $("#" + element.wrapper).append(element.pages);
                    }
                }
                slidelog("childs loaded");
            });

            element._prepareEventHanler();
        };

        element._prepareChilds = function(callback){
            var childs = element.children();

            var x = 0;
            childs.each(function () {
                var child = $(this), img, url, captions;

                if (child.is('img')) {
                    img = child;
                    url = '#';
                } else if (child.is('a')) {
                    img = child.find("img:first");
                    url = child.attr("href");
                } else {
                    img = child.find("img:first");
                    url = (child.find("a:first").length != 0 ? child.find("a:first").attr("href") : '#');
                }
                captions = (child.find(".captionArea:first").length != 0 ? child.find(".captionArea:first") : null);

                if (img.prop("tagName").toLowerCase() !== "img") {
                    child.remove();
                }
                else{
                    element.childsData[x] = {
                        "img": img,
                        "url": url,
                        "src": img.attr("src"),
                        'iwidth': img.width(),
                        'iheight': img.height(),
                        'captions': captions,
                        'captioninit': null,
                        'captionRun': false,
                    };
                    img.css({"width": "100%"});
                    child.attr("data-width", element.childsData[x].iwidth)
                        .attr("data-height", element.childsData[x].iheight)
                        .attr("data-src", element.childsData[x].src)
                        .attr("data-href", element.childsData[x].url)
                        .css({width: '100%', display: 'none'});
                    x++;
                    element.pages.append('<li><a>' + x + '</a></li>');
                }
            });

            element.totalChilds = (childs.size() - 1);
            /*nextSlide methodu 1 artırıp gösterim yapacak, kullanıcının kendi seçimini göstermek için belirtilenin 1 altında değer olması lazım*/
            element.vars.currentindex--;
            if (element.vars.currentindex > element.totalChilds || element.vars.currentindex < 1) {
                element.dVars.activeIndex = -1;
            }
            callback(childs);
        };

        element._prepareEventHanler = function () {

            $(window).resize(function(){
                element.fixResize();
                element.vars.resizeCall(element);
            });

            if(element.vars.pauseOnHover === true){
                slidelog("init hover event");
                $(document).on('mouseenter', "#"+element.wrapper, function() {
                    element.pauseProcess();
                    slidelog("hover pause");
                    if(element.vars.controlNav === true){
                        $("#"+element.wrapper).find(".mtmSlideHover").stop(true,true).fadeIn("normal");
                    }
                });
                $(document).on('mouseleave', "#"+element.wrapper, function() {
                    element.playProcess();

                    slidelog("hover out play");
                    if(element.vars.controlNav === true){
                        $("#"+element.wrapper).find(".mtmSlideHover").stop(true,true).fadeOut("normal");
                    }
                });

            }

            if(element.vars.controlNav === true){
                $(document).on('click', "#"+element.wrapper+"-pages > li > a", function () {
                    slidelog("goto slide "+$(this).parent("li").index());
                    element.gotoSlide($(this).parent("li").index());
                });
                $(document).on('click', "#"+element.wrapper+"-arrows > a", function () {
                    if($(this).hasClass("mtmPrevious")){
                        slidelog("previous slide click");
                        element.previousSlide();
                    }
                    else{
                        slidelog("next slide click");
                        element.nextSlide();
                    }
                });

                $(document).on('click', element.vars.previousLink,function(){ $(".mtmPrevious",element.vars.control).trigger("click"); });
                $(document).on('click', element.vars.nextLink,function(){ $(".mtmNext",element.vars.control).trigger("click"); });
            }

            if(element.vars.touchControl === true){
                $('img', element).on('dragstart', function(event) { event.preventDefault(); return false; });
                $("#"+element.wrapper).mtmMouseMoveEvent({
                    leftCall:function(e){
                        slidelog("touch previous slide");
                        element.previousSlide();
                    },
                    rightCall:function(e){
                        slidelog("touch next slide");
                        element.nextSlide();
                    }
                });
            }
        };

        return element.initialize();

        /*########## EFFECT RUN #########*/
        function effectRun(){
            var vars = element.childsData[element.vars.currentindex];

            slidelog(vars.effect+ " effect start. current index: "+element.vars.currentindex);

            switch (vars.effect){
                case "slideFade":
                    slideFade(vars);
                    break;
                case "slideZoomOut":
                    slideZoomOut(vars);
                    break;
                case "cssAnimate":
                    cssAnimate(vars);
                    break;
                case "slideMosaicLeft":
                case "slideMosaicRight":
                case "slideMosaicCenter":
                case "slideMosaicShuffle":
                    slideMosaic(vars);
                    break;
                case "slideVertical":
                case "slideHorziontal":
                    slideSlices(vars);
                    break;
                default :
                    slideFade(vars);
            }
        }

        /*########## EFFECT START #########*/

        function slideFade(vars){
            $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
            $("li",element).eq(vars.liindex).fadeIn("normal").addClass("active");
            /** consoleWrite("slideFade metodu calisti"); /**/

            element.slideEnd();
        }

        function slideZoomOut(vars){
            var zoomLi = $(".active",element).html();
            var zoomDiv = $('<div class="zoomDiv removeSlideBox"></div>');
            zoomDiv.html(zoomLi);
            $("#"+element.wrapper).append(zoomDiv);
            var zw = (vars.currentW / 3);
            var mw = (zw / 2);
            var zh = (vars.replaceH / 3);
            var mh = (zh / 2);
            zoomDiv.animate({width:(vars.currentW + zw)+"px", height:(vars.replaceH + zh)+"px",marginLeft:-mw+"px",marginTop:-mh+"px",opacity:"0"},element.vars.animSpeed,function(){ zoomDiv.remove(); });
            $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
            $("li",element).eq(vars.liindex).fadeIn("normal").addClass("active");
            /** consoleWrite("slideZoomOut metodu calisti");/**/

            element.slideEnd();
        }

        function cssAnimate(vars){

            $("li",element).not(":eq( "+vars.liindex+" )").removeClass("active");
            $("li",element).eq(vars.liindex).addClass("active").addClass('animated')
                .addClass(vars.cssAnimate)
                .css({visibility:'visible'})
                .show();

            element.slideEnd();

            $("li",element).eq(vars.liindex).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $("li",element).eq(vars.liindex).removeClass('animated').removeClass(vars.cssAnimate);
                $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal");
            });

        }

        function slideSlices(vars){

            var mozTimer;

            var sliceW = Math.round(vars.currentW / element.vars.hSlices);
            var sliceH = Math.round(vars.replaceH / element.vars.vSlices);

            var mosBox = $('<div class="mosBox removeSlideBox"></div>');
            $("#"+element.wrapper).append(mosBox);

            var i = 0;
            if(vars.effect == "slideVertical"){
                for(var y = 0; y < element.vars.vSlices; y++){
                    var icDiv =  $('<div class="mosInDiv"></div>');
                    icDiv.addClass("mosMos"+i);
                    icDiv.css({
                        position: 'absolute',
                        width: '100%',
                        height: sliceH,
                        marginTop: (y * sliceH),
                        marginLeft: 0,
                        backgroundImage: 'url(' + vars.src+ ')',
                        backgroundRepeat : 'no-repeat',
                        backgroundSize : vars.currentW+'px '+vars.replaceH+'px',
                        backgroundPosition: '0 ' + -(y * sliceH) + 'px',
                        overflow: 'hidden',
                        display : 'none',
                        zIndex: 9999
                    });
                    mosBox.append(icDiv);
                    i++;
                }

                i = 0;
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(i).addClass('animated')
                        .addClass(vars.cssAnimate)
                        .css('visibility', 'visible')
                        .show();
                    if(i >= element.vars.vSlices){
                        clearInterval(mozTimer);
                        setTimeout(function(){
                            $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                            $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        },(element.vars.vSlices * 150));
                        element.slideEnd();
                        return false;
                    }
                    i++
                },150);

            }else{
                for(var x = 0; x < element.vars.hSlices; x++){
                    var icDiv =  $('<div class="mosInDiv"></div>');
                    icDiv.addClass("mosMos"+i);
                    icDiv.css({
                        position: 'absolute',
                        width: sliceW,
                        height: '100%',
                        marginTop: 0,
                        marginLeft: (x * sliceW),
                        backgroundImage: 'url(' + vars.src+ ')',
                        backgroundRepeat : 'no-repeat',
                        backgroundSize : vars.currentW+'px '+vars.replaceH+'px',
                        backgroundPosition: -(x * sliceW) + 'px 0',
                        overflow: 'hidden',
                        display : 'none',
                        zIndex: 9999
                    });
                    mosBox.append(icDiv);
                    i++;
                }
                i = 0;
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(i).addClass('animated')
                        .addClass(vars.cssAnimate)
                        .css('visibility', 'visible')
                        .show();
                    if(i >= element.vars.hSlices){
                        clearInterval(mozTimer);
                        setTimeout(function(){
                            $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                            $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        },(element.vars.vSlices * 150));
                        element.slideEnd();
                        return false;
                    }
                    i++
                },150);
            }
        }

        function slideMosaic(vars){

            var mozTimer;

            var sliceW = Math.round(vars.currentW / element.vars.hSlices);
            var sliceH = Math.round(vars.replaceH / element.vars.vSlices);

            var mosBox = $('<div class="mosBox removeSlideBox"></div>');
            $("#"+element.wrapper).append(mosBox);
            var mosBoxIn = new Array;
            var i = 0;
            for(var x = 0; x < element.vars.hSlices; x++){
                for(var y = 0; y < element.vars.vSlices; y++){
                    var icDiv =  $('<div class="mosInDiv"></div>');
                    icDiv.addClass("mosMos"+i);
                    icDiv.css({
                        position: 'absolute',
                        width: sliceW,
                        height: sliceH,
                        marginTop: (y * sliceH),
                        marginLeft: (x * sliceW),
                        backgroundImage: 'url(' + vars.src+ ')',
                        backgroundRepeat : 'no-repeat',
                        backgroundSize : vars.currentW+'px '+vars.replaceH+'px',
                        backgroundPosition: -(x * sliceW) + 'px ' + -(y * sliceH) + 'px',
                        overflow: 'hidden',
                        display : 'none',
                        zIndex: 9999
                    });
                    mosBox.append(icDiv);
                    i++;
                }
            }
            if(vars.effect == "slideMosaicRight"){

                i = (element.vars.hSlices * element.vars.vSlices);
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(i).fadeIn("slow");
                    if(i < 0){
                        clearInterval(mozTimer);
                        $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                        $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        element.slideEnd();
                        return false;
                    }
                    i--
                },20);

            }else if(vars.effect == "slideMosaicCenter"){
                x = Math.round((element.vars.hSlices * element.vars.vSlices) / 2);
                i = x; /*((vars.hSlices * vars.vSlices) / 2);*/
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(i).fadeIn("slow");
                    $(".mosInDiv",mosBox).eq(x).fadeIn("slow");

                    if(x > (element.vars.hSlices * element.vars.vSlices)){
                        clearInterval(mozTimer);
                        $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                        $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        element.slideEnd();
                        return false;
                    }
                    i--;
                    x++;
                },35);
            }else if(vars.effect == "slideMosaicShuffle"){
                var arr = new Array((element.vars.hSlices * element.vars.vSlices));
                arr = array_keys(arr);
                arr = shuffle(arr);
                i = 0;
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(arr.shift()).fadeIn("slow");

                    if(i >= (element.vars.hSlices * element.vars.vSlices)){
                        clearInterval(mozTimer);
                        $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                        $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        element.slideEnd();
                        return false;
                    }
                    i++
                },20);
            }else{
                i = 0;
                mozTimer = setInterval(function(){
                    $(".mosInDiv",mosBox).eq(i).fadeIn("slow");
                    if(i >= (element.vars.hSlices * element.vars.vSlices)){
                        clearInterval(mozTimer);
                        $("li",element).not(":eq( "+vars.liindex+" )").fadeOut("normal").removeClass("active");
                        $("li",element).eq(vars.liindex).fadeIn("normal",function(){ mosBox.remove(); }).addClass("active");
                        element.slideEnd();
                        return false;
                    }
                    i++
                },20);
            }
        }
        /*########## EFFECT END #########*/        


    }
})(jQuery);

function slidelog(e)  { if (window.location.host === "slide.test") { console.log(e); } }
function findHeightProportions(x,y,z){/* (x(width) - y(height)) | (z(width) - n(? height)) */ return ((y * z) / x);}
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}
function array_keys(o){
    o = shuffle(o);
    var r = new Array(0);
    for (var i in o){
        r.push(i);
    }
    return r;
}
