<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Mavitm Layer Slider</title>
    <link href="src/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="src/css/mtmslider_all.css" rel="stylesheet" type="text/css" />
    <style>
        body{
            background-color: #cccccc;
        }
    </style>
</head>
<body>
<?php /**/?>
<div id="boxSlide">
    <ul id="mavitmSlider">
        <?php /**/?>
        <li data-animation="slideZoomOut"><a href="#"><img src="images/1.jpg" alt="" /></a></li>

        <li><a href="#"><img src="images/3.jpg" alt="" /></a>
            <div class="captionArea">
                <div class="caption"
                     data-in-speed="1000ms"
                     data-in-effect="zoomInUp"
                     data-out-speed="1000ms"
                     data-out-effect="bounceOutDown"
                     data-in-delay="100ms"
                     data-out-delay="5000ms"
                     data-position-x="25"
                     data-position-y="25"
                     data-position-z="500"
                     data-textsplit="true"><h1>Gandalf bastonu ile büyü yapar</h1></div>
                <div class="caption"
                     data-in-speed="1000ms"
                     data-in-effect="slideInDown"
                     data-out-speed="1000ms"
                     data-out-effect="bounceOut"
                     data-in-delay="1100ms"
                     data-out-delay="6000ms"
                     data-position-x="25"
                     data-position-y="50"
                     data-position-z="500"><h3>Çakma gandalf sütlaç köyünde yaşar</h3></div>
            </div>
        </li>

        <li><a href="#"><img src="images/2.jpg" alt="" /></a></li>
        <?php /**/?>
    </ul>
</div>

<?php /*
<script type="text/javascript" src="src/js/jquery.mtmLayer.js"></script>
<script type="text/javascript" src="src/js/jquery.mtmMouseEvents.js"></script>
<script type="text/javascript" src="src/js/jquery.mtmAttributes.js"></script>
<script type="text/javascript" src="src/js/jquery.mtmSlider.js"></script>
 */?>

<script type="text/javascript" src="src/js/jquery.js"></script>
<script type="text/javascript" src="src/js/mtmslider_all.js"></script>

    <script type="text/javascript">
        $(window).ready(function(){
            var mtmSLide = $("#mavitmSlider").mavitmSlider();
        });
    </script>
</body>
</html>